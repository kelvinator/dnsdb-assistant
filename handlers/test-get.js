const dnsdb = require('../services/dnsdb-api');
const utils = require('../services/util')

const handler = (req, res) => {
    let datePeriod =  {
        "startDate": "2019-05-01T00:00:00",
        "endDate": "2019-07-31T23:59:59"
    }
    dnsdb.lookupRRSet('watchaturtle.com', 'SOA', utils.getUnixTimeStamp(datePeriod.startDate), 
        utils.getUnixTimeStamp(datePeriod.endDate)).then(result => {
        
            if(result.data) {
                console.log(result.data);
            }        
            res.status(200).send('huh');
    });
}

module.exports.handler = handler;