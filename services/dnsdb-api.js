const rpn = require('request-promise-native');
const db = require('./db-util');

const baseURL = 'https://api.dnsdb.info';

const lookupRRSet = (domain, rRType, timeFirstAfter, timeLastBefore) => {
    let url = `${baseURL}/lookup/rrset/name/${domain}/${rRType}/?limit=100`;
    
    if(timeFirstAfter) {
        url += `&time_first_after=${timeFirstAfter}`;
    }

    if(timeLastBefore) {
        url += `&time_last_before=${timeLastBefore}`
    }

    const options = {
        method: 'GET',
        uri: url,
        headers: {
            'X-API-Key': db.getAPIKey(null),
            'Accept': 'application/json'
        },
        resolveWithFullResponse: true,
        transform: processJSON,
        transform2xxOnly: true
    }

    return rpn(options).catch((err) => {
        console.error(err.statusCode);
        console.error(err.message);
        return {
            statusCode: +err.statusCode,
            message: err.message,
            data: null
        };
    });
}

const processJSON = (jsonBody, response) => {
    const data = jsonBody.split('\n').slice(0, -1).map(e => {
        return JSON.parse(e);
    });
    return {
        statusCode: response.statusCode,
        message: undefined,
        data: data
    };
}

module.exports = {
    lookupRRSet: lookupRRSet
}