
const getAPIKey = (userId) => {
    const apiKey = process.env.APIKEY 
    return apiKey;
}

module.exports = {
    getAPIKey: getAPIKey
}