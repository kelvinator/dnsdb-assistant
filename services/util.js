const domainDataParser = (domain) => {
    result = domain.split(' ').join('');
    return domain;
}

const getUnixTimeStamp =  (dateString) => {
    if(dateString.length > 19) {
        dateString = dateString.substring(0, 19);
    }
    const dateObj = new Date(dateString);
    return Math.floor(dateObj.getTime()/1000);
}

module.exports = {
    domainDataParser: domainDataParser,
    getUnixTimeStamp: getUnixTimeStamp
}