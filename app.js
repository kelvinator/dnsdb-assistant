require('dotenv').config()
const express = require('express');
const cors = require('cors')({origin: true});
const bodyParser = require('body-parser');
const rp = require('request-promise-native');
const {
    dialogflow,
    Suggestions
  } = require('actions-on-google');

const app = express();
app.use(bodyParser.json());
app.use(cors);

const dialogFlowApp = dialogflow({debug: false});

app.get('/', (req, res) => {
    res.status(200).send(`Hello from the dndb-assistant server! The current time is: ${Date.now()}`);
});

const testGet = require('./handlers/test-get');
app.get('/testget', testGet.handler);

const rRSetQuery = require('./handlers/rrset-query');
dialogFlowApp.intent('rrset query', rRSetQuery.handler);

dialogFlowApp.intent('test intent', (conv, params) => {
    // Use the number of reprompts to vary response
    // console.log(url);
    // console.log(rRType);
    console.log(`This is it!!!!!!!!!!! ${JSON.stringify(params, null, 2)}`)
    conv.close(`Great thank you.`);
});

app.post('/dialogflow', dialogFlowApp);

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`App listening on port ${PORT}`);
    if(!process.env.APIKEY) {
        console.error('No APIKEY! You need to set it in a .env file!');
    }
});
